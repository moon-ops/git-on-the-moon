Git - Historique
---

Affiche les logs de commit: `git log` <br>

Retourne à un moment précis: `git checkout <hash commit>`
